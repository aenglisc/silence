FROM elixir:1.10.4-alpine as build

WORKDIR /script

COPY ./lib ./lib
COPY ./mix.exs .
COPY ./mix.lock .

RUN mix local.hex --force && \
    mix local.rebar --force && \
    mix deps.get --only prod && \
    MIX_ENV=prod mix escript.build

FROM erlang:23.1.1.0-alpine

WORKDIR /script

COPY --from=build /script/silence .

ENTRYPOINT ["/script/silence"]
