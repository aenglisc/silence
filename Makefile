all: setup check escript

docker:
	docker build -t silence .

get-deps:
	mix deps.get

compile:
	mix compile

escript:
	mix escript.build

test:
	mix test

dialyzer:
	mix dialyzer

clean:
	rm -rf _build deps silence .elixir_ls

check: dialyzer test

setup: get-deps compile

.PHONY: test
