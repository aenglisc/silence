defmodule SilenceTest do
  use ExUnit.Case

  alias Silence.{
    Builder.Segment
  }

  @example_file "examples/silence1.xml"
  @test_opts [
    chapter_split: 4,
    segment_split: 2,
    max_segment_length: 1000
  ]

  test "extracts durations" do
    durations =
      @example_file
      |> File.read!()
      |> Silence.Parser.extract_durations()

    assert length(durations) == 941
  end

  test "builds segments" do
    segments =
      @example_file
      |> File.read!()
      |> Silence.Parser.extract_durations()
      |> Silence.Builder.build_segments()

    assert length(segments) == 139

    assert [
             %Segment{chapter: 1, part: nil, from: "PT0S"},
             %Segment{chapter: 2, part: nil}
             | _
           ] = segments
  end

  test "builds segments with opts" do
    segments =
      @example_file
      |> File.read!()
      |> Silence.Parser.extract_durations()
      |> Silence.Builder.build_segments(@test_opts)

    assert length(segments) == 253

    assert [
             %Segment{chapter: 1, part: 1, from: "PT0S"},
             %Segment{chapter: 1, part: 2, from: "PT15M55.05S"}
             | _
           ] = segments
  end

  test "formats json with opts" do
    json =
      @example_file
      |> Silence.split(@test_opts)

    assert length(Jason.decode!(json)["segments"]) == 253

    assert %{
             "segments" => [
               %{"offset" => "PT0S", "title" => "Chapter 1, part 1"},
               %{"offset" => "PT15M55.05S", "title" => "Chapter 1, part 2"}
               | _
             ]
           } = Jason.decode!(json)
  end
end
