defmodule Silence do
  @moduledoc """
  Documentation for `Silence`.
  """

  alias Silence.{
    Parser,
    Builder,
    Formatter
  }

  @spec split(path :: String.t(), Builder.opts()) :: Formatter.output()
  def split(path, opts \\ []) do
    path
    |> File.read!()
    |> Parser.extract_durations()
    |> Builder.build_segments(opts)
    |> Formatter.format()
  end
end
