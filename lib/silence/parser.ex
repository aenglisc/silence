defmodule Silence.Parser do
  @initial "PT0S"
  @type duration() :: binary()
  @type xml() :: binary()

  @spec extract_durations(xml) :: [Silence.Time.duration()]
  def extract_durations(xml) do
    xml
    |> SAXMap.from_string(ignore_attribute: false)
    |> unwrap()
    |> get_content()
    |> Enum.flat_map(&parse_durations/1)
    |> insert_initial()
  end

  @spec unwrap({:ok, map()} | {:error, any()}) :: map() | no_return()
  defp unwrap({:ok, result}), do: result
  defp unwrap({:error, e}), do: throw(e)

  @spec get_content(map()) :: map()
  defp get_content(data) do
    data
    |> Map.fetch!("silences")
    |> Map.fetch!("content")
    |> Map.fetch!("silence")
  end

  @spec parse_durations(map()) :: [Silence.Time.duration()]
  defp parse_durations(%{"from" => from, "until" => until}) do
    [from, until]
  end

  @spec insert_initial([Silence.Time.duration()]) :: [Silence.Time.duration()]
  defp insert_initial([@initial | _rest] = timestamps), do: timestamps
  defp insert_initial(timestamps), do: [@initial | timestamps]
end
