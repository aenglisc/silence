defmodule Silence.Formatter do
  alias Silence.Builder.Segment

  defmodule Formattable do
    @type t() :: %Formattable{
            title: String.t(),
            offset: Silence.Time.duration()
          }

    @derive Jason.Encoder
    defstruct [
      :title,
      :offset
    ]
  end

  @type output() :: String.t()

  @spec format(segments :: [Segment.t()]) :: output()
  def format(segments) do
    %{"segments" => Enum.map(segments, &segment_to_formattable/1)}
    |> Jason.encode!(pretty: true)
  end

  @spec segment_to_formattable(Segment.t()) :: Formattable.t()
  defp segment_to_formattable(segment) do
    %Formattable{
      offset: segment.from,
      title: build_title(segment)
    }
  end

  @spec build_title(Segment.t()) :: String.t()
  defp build_title(%Segment{chapter: chapter, part: nil}), do: "Chapter #{chapter}"
  defp build_title(%Segment{chapter: chapter, part: part}), do: "Chapter #{chapter}, part #{part}"
end
