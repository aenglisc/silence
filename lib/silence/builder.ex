defmodule Silence.Builder do
  defmodule Chunk do
    @type t() :: %Chunk{
            silence?: boolean(),
            from: Silence.Time.duration(),
            until: Silence.Time.duration() | nil,
            duration: Silence.Time.seconds()
          }

    defstruct [
      :silence?,
      :from,
      :until,
      :duration
    ]
  end

  defmodule Segment do
    @type t() :: %Segment{
            chapter: pos_integer(),
            part: pos_integer() | nil,
            chunks: [Chunk.t()],
            from: Silence.Time.duration(),
            duration: Silence.Time.seconds()
          }

    defstruct chapter: 1,
              part: nil,
              chunks: [],
              from: nil,
              duration: 0
  end

  @default_chapter_split 3
  @default_segment_split 1
  @default_max_segment_length 1500

  @type opts :: Keyword.t()

  @spec build_segments([Silence.Time.duration()], opts()) :: [Segment.t()]
  def build_segments(durations, opts \\ []) do
    durations
    |> build_chunks()
    |> build_segments_(opts)
  end

  @spec build_chunks([Silence.Time.duration()], boolean()) :: [Chunk.t()]
  defp build_chunks(durations, silence? \\ false) do
    case durations do
      [from, until | rest] ->
        chunk = %Chunk{
          silence?: silence?,
          from: from,
          until: until,
          duration: Silence.Time.diff(from, until)
        }

        [chunk | build_chunks([until | rest], not chunk.silence?)]

      [from] ->
        [%Chunk{silence?: silence?, from: from, duration: 0}]
    end
  end

  @spec build_segments_([Chunk.t()], opts()) :: [Segment.t()]
  defp build_segments_([chunk | chunks], opts) do
    initial_segment = %Segment{
      chunks: [chunk],
      from: chunk.from,
      duration: chunk.duration
    }

    chunks
    |> Enum.reduce([initial_segment], &build_segment(&1, &2, opts))
    |> Enum.sort_by(&{&1.chapter, &1.part}, :asc)
  end

  @spec build_segment(Chunk.t(), [Segment.t()], opts()) :: [Segment.t()]
  defp build_segment(chunk, [segment | segments], opts) do
    chapter_split = Keyword.get(opts, :chapter_split, @default_chapter_split)
    segment_split = Keyword.get(opts, :segment_split, @default_segment_split)
    max_segment_length = Keyword.get(opts, :max_segment_length, @default_max_segment_length)

    chunk_duration = chunk.duration
    segment_duration = segment.duration
    segment_chapter = segment.chapter
    segment_part = segment.part

    case chunk do
      %Chunk{silence?: true} when chunk_duration >= chapter_split ->
        new_segment = %Segment{chapter: segment_chapter + 1, chunks: [], from: chunk.until}
        [new_segment, segment | segments]

      %Chunk{silence?: true} when chunk_duration >= segment_split and not is_nil(segment_part) ->
        new_segment = %Segment{
          chapter: segment_chapter,
          part: segment_part + 1,
          chunks: [],
          from: chunk.until
        }

        [new_segment, segment | segments]

      %Chunk{} when chunk_duration + segment_duration > max_segment_length ->
        part = if is_nil(segment_part), do: 1, else: segment_part

        new_segment = %Segment{
          chapter: segment_chapter,
          part: part + 1,
          duration: segment_duration,
          chunks: if(chunk.silence?, do: [], else: [chunk]),
          from: if(chunk.silence?, do: chunk.until, else: chunk.from)
        }

        [new_segment, %Segment{segment | part: part} | segments]

      %Chunk{} when chunk_duration + segment_duration <= max_segment_length ->
        updated_segment = %Segment{
          segment
          | duration: chunk_duration + segment_duration,
            chunks: [chunk | segment.chunks]
        }

        [updated_segment | segments]
    end
  end
end
