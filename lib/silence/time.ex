defmodule Silence.Time do
  @timestamp :calendar.now_to_datetime(:erlang.now())

  @type duration() :: String.t()
  @type seconds() :: non_neg_integer()

  # I guess floats might have been better

  @spec diff(from :: duration(), until :: duration()) :: seconds()
  def diff(from, until) do
    :calendar.time_difference(to_datetime(from), to_datetime(until))
    |> daystime_to_time()
    |> :calendar.time_to_seconds()
  end

  @spec to_datetime(duration()) :: :calendar.datetime()
  defp to_datetime(duration) do
    # passing a binary causes dialyzer issues
    :iso8601.apply_duration(@timestamp, :erlang.binary_to_list(duration))
  end

  @spec to_datetime({days :: integer(), :calendar.time()}) :: :calendar.time()
  defp daystime_to_time({_, time}), do: time
end
