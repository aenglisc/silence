defmodule Silence.CLI do
  def main(args) do
    parsed_args = parse_args(args)
    apply(&do_task/2, parsed_args)
  end

  defp parse_args([path]), do: [path, []]

  defp parse_args([path, chapter_split]) do
    [
      path,
      [
        chapter_split: String.to_integer(chapter_split)
      ]
    ]
  end

  defp parse_args([path, chapter_split, max_segment_length]) do
    [
      path,
      [
        chapter_split: String.to_integer(chapter_split),
        segment_length: String.to_integer(max_segment_length)
      ]
    ]
  end

  defp parse_args([path, chapter_split, max_segment_length, segment_split]) do
    [
      path,
      [
        chapter_split: String.to_integer(chapter_split),
        max_segment_length: String.to_integer(max_segment_length),
        segment_split: String.to_integer(segment_split)
      ]
    ]
  end

  defp do_task(path, opts) do
    path
    |> Silence.split(opts)
    |> IO.puts()
  end
end
