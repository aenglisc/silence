# Silence

A simple CLI that takes an xml file with silence durations and splits it into book segments.

## Arguments
* `path_to_xml`
* `chapter_split`: seconds of silence delimiting a chapter
* `max_segment_length`: how long a chapter should go for before being split, in seconds
* `segment_split`: seconds of silence delimiting a segment within a chapter

## Usage

* `make` setup, run checks, build escript
* `make check` run tests an dialyzer
* `make clean` remove all the dependencies and the escript
* `make docker` build an image with the escript
* ``docker run --rm -v `pwd`:/script silence [ARGS...]`` to run via docker.
